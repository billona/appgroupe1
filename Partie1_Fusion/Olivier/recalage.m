function [P1icp, C1icp] = recalage(P1, C1, P2, C2)
% Effectue l'ICP (Iterative Closest Point) sur deux nuages de points :
% transformation geometrique que doit subir P1 pour etre au meme endroit que P2.
% P1 (n*3 single) : coordonnees du 1er nuage de points
% C1 (n*3 uint8) : couleurs du 1er nuage de points
% P2 (n*3 single) : coordonnees du 2nd nuage de points
% C2 (n*3 uint8) : couleurs du 2nd nuage de points
% P1icp (n*3 single) : coordonnees du 1er nuage de points ayant subi l'ICP
% C1icp (n*3 uint8) : couleurs du 1er nuage de points ayant subi l'ICP

% ICP entre les vues 1 et 2
[Ricp, Ticp] = icp(P1', P2', 10, "Matching", "kDtree");
% M = pcregistericp(P2, P1, 'Extrapolate', true);
% M = pcregrigid(pointCloud(P2), pointCloud(P1), 'Extrapolate', true);
% M = M.T;
% Ricp = M(1:3, 1:3);
% Ticp = M(4, 1:3)';

% Modification de la vue 1
P1icp = Ricp * P1' + repmat(Ticp, 1, size(P1, 1));
P1icp = P1icp';
C1icp = C1;