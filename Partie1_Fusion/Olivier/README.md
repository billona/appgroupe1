## Modèle utilisé

Il s'agit d'une figurine du personnage [Jon Snow](https://fr.wikipedia.org/wiki/Jon_Snow) (de la série télévisée [Game of Thrones](https://fr.wikipedia.org/wiki/Game_of_Thrones)).
![Modèle](https://images-na.ssl-images-amazon.com/images/I/41m99K46P1L.jpg)

## Exécuter les programmes

### Afficher successivement les dix vues de Jon Snow
Lancer le script [`afficher_jon_snow.m`](Partie1_Fusion/Olivier/afficher_jon_snow.m)

### Recaler les dix vues de Jon Snow en une seule image (et créer un fichier `.ply` de ce nuage de points)
Lancer le script [`recalage_jon_snow.m`](Partie1_Fusion/Olivier/recalage_jon_snow.m)