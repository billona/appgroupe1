% Effectue le recalage des vues de Jon Snow.

close all;
path = "./Jon_Snow/";

% Deux premieres vues
[P1, C1] = afficher_points(path + "1.ply", false);
[P2, C2] = afficher_points(path + "2.ply", false);
[Picp, Cicp] = recalage(P1, C1, P2, C2);

% Autres vues
for i = 3:10
    fichier = path + i + ".ply";
    [Pi, Ci] = afficher_points(fichier, false);
    
    [Picp_new, Cicp_new] = recalage(Picp, Cicp, Pi, Ci);
    Picp = [Picp; Picp_new];
    Cicp = [Cicp; Cicp_new];
end

figure;
scatter3(Picp(:, 1), Picp(:, 2), Picp(:, 3), 1, [double(Cicp(:, 1)) double(Cicp(:, 2)) double(Cicp(:, 3))] ./ 255), view(0, -90)
title("Recalage vues 1 a 10");

% K-means sur les couleurs LAB du recalage
C_LAB = rgb2lab(Cicp);
[ind, ~] = kmeans(C_LAB, 4);
classe_1 = find(ind == 1);
classe_2 = find(ind == 2);
classe_3 = find(ind == 3);
classe_4 = find(ind == 4);
P1 = Picp([classe_1], :);
C1 = Cicp([classe_1], :);
P2 = Picp([classe_2], :);
C2 = Cicp([classe_2], :);
P3 = Picp([classe_3], :);
C3 = Cicp([classe_3], :);
P4 = Picp([classe_4], :);
C4 = Cicp([classe_4], :);

% figure;
% scatter3(P1(:, 1), P1(:, 2), P1(:, 3), 1, [double(C1(:, 1)) double(C1(:, 2)) double(C1(:, 3))] ./ 255), view(0, -90);
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title("Classe 1 apres recalage vues 1 a 10");
% 
% figure;
% scatter3(P2(:, 1), P2(:, 2), P2(:, 3), 1, [double(C2(:, 1)) double(C2(:, 2)) double(C2(:, 3))] ./ 255), view(0, -90);
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title("Classe 2 apres recalage vues 1 a 10");
% 
% figure;
% scatter3(P3(:, 1), P3(:, 2), P3(:, 3), 1, [double(C3(:, 1)) double(C3(:, 2)) double(C3(:, 3))] ./ 255), view(0, -90);
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title("Classe 3 apres recalage vues 1 a 10");
% 
% figure;
% scatter3(P4(:, 1), P4(:, 2), P4(:, 3), 1, [double(C4(:, 1)) double(C4(:, 2)) double(C4(:, 3))] ./ 255), view(0, -90);
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title("Classe 4 apres recalage vues 1 a 10");
% 
% Pfinal = [P2; P3; P4];
% Cfinal = [C2; C3; C4];
% 
% figure;
% scatter3(Pfinal(:, 1), Pfinal(:, 2), Pfinal(:, 3), 1, [double(Cfinal(:, 1)) double(Cfinal(:, 2)) double(Cfinal(:, 3))] ./ 255), view(0, -90);
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title("Classes 1 a 3 apres recalage vues 1 a 10");

% Ecriture dans un fichier .ply
write_ply("jon_snow.ply", Picp', Cicp');
