function [P, C] = afficher_points(fichier, graphe)
% Renvoie les points et couleurs du modele contenu dans le fichier fichier.
% fichier (string) : chemin du fichier a afficher
% graphe (bool) : indique si on veut afficher le graphe ou non
% P (n*3 single) : coordonnees du nuage de points
% C (n*3 uint8) : couleurs du nuage de points

% close all;

nuage = pcread(fichier);

if ~graphe
    nuage = pcdownsample(nuage, 'gridAverage', 0.002);
    % nuage = pcdownsample(nuage, 'random', 0.05);
end

P = nuage.Location;
C = nuage.Color;

% Exclusion du fond
ind = find(P(:, 3) < 0.4 & P(:, 3) > 0.3);
P = P(ind, :);
C = C(ind, :);

% Exclusion des couleurs bleue et verte
% noir = (C(:, 1) < 50) & (C(:, 2) < 50) & (C(:, 3) < 50);
noir = C(:, 1) + C(:, 2) + C(:, 3) < 200;
% bleu = (C(:, 3) - C(:, 1) > 20) & (C(:, 3) - C(:, 2) > 20);
bleu = (C(:, 1) > 10 & C(:, 1) < 105 & C(:, 2) > 25 & C(:, 2) < 125 & C(:, 3) > 65 & C(:, 3) < 185);
% vert = (C(:, 2) - C(:, 1) > 20) & (C(:, 2) - C(:, 3) > 20);
vert = (C(:, 1) > 60 & C(:, 1) < 120 & C(:, 2) > 95 & C(:, 2) < 155 & C(:, 3) > 80 & C(:, 3) < 140);
ind = find(~bleu & ~vert | noir & ~bleu & ~vert);
P = P(ind, :);
C = C(ind, :);

% Exclusion des points eloignes du modele
% ind = find(abs(P(:, 1)) < 1);
% P = P(ind, :);
% C = C(ind, :);

% K-means
% C_LAB = rgb2lab(C);
% [ind, ~] = kmeans(C_LAB, 4);
% classe_1 = find(ind == 1);
% classe_2 = find(ind == 2);
% classe_3 = find(ind == 3);
% classe_4 = find(ind == 4);
% P1 = P([classe_1], :);
% C1 = C([classe_1], :);
% P2 = P([classe_2], :);
% C2 = C([classe_2], :);
% P3 = P([classe_3], :);
% C3 = C([classe_3], :);
% P4 = P([classe_4], :);
% C4 = C([classe_4], :);
% 
% P = [P1; P2; P4];
% C = [C1; C2; C4];

if graphe
    scatter3(P(:, 1), P(:, 2), P(:, 3), 1, [double(C(:, 1)) double(C(:, 2)) double(C(:, 3))] ./ 255), view(0, -90)
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
end

end