% Affiche les 10 vues du modele Jon Snow.

close all;
path = "./Jon_Snow/";

for i = 1:10
    fichier = path + i + ".ply";
    [P, C] = afficher_points(fichier, true);
    title("Vue " + i);
    pause(1.5);
end
