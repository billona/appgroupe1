#APP OPENCV Project

#How to

OPENCV:
Linux installation only : https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html#linux-installation
First Example: https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_gcc_cmake/linux_gcc_cmake.html#linux-gcc-usage

PCL:
Download:	https://github.com/PointCloudLibrary/pcl/releases
COMPILE:	http://www.pointclouds.org/documentation/tutorials/compiling_pcl_posix.php
DOC:		http://docs.pointclouds.org/trunk/classpcl_1_1_p_l_y_reader.html


