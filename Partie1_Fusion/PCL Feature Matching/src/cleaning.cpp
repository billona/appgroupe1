#include<cleaning.h>


namespace clean{
	
	Procstep::Procstep() :
		cloudout(new pcl::PointCloud<pcl::PointXYZRGB>())
	{
	}
	
	Filter::Filter(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudin_, pcl::FieldComparison<pcl::PointXYZRGB> cmp):
		Procstep(),
		cloudin(cloudin_),
		cond(new pcl::ConditionOr<pcl::PointXYZRGB> ())
	{
		
	//cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZRGB> ("z", pcl::ComparisonOps::LT, 1.0)));
	cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr(&cmp));
	
	
	//do the filtering inside construction in list
		pcl::ConditionalRemoval<pcl::PointXYZRGB> condrem;
		condrem.setCondition(cond);
		condrem.setInputCloud (cloudin);
		condrem.setKeepOrganized(true);
		condrem.filter (*cloudout);

	}

}
