
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/centroid.h>

#include <pcl/point_types.h>
#include <pcl/filters/conditional_removal.h>


namespace clean{

class Filter{
public:
	Filter(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudin, pcl::FieldComparison<pcl::PointXYZRGB> cmp):
	cond(new pcl::ConditionOr<pcl::PointXYZRGB> ()),
	cloudout(new pcl::PointCloud<pcl::PointXYZRGB>())
	{
		
	//cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZRGB> ("z", pcl::ComparisonOps::LT, 1.0)));
	cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr(&cmp));

	// build the filter
	pcl::ConditionalRemoval<pcl::PointXYZRGB> condrem;
	condrem.setCondition(cond);
	condrem.setInputCloud (cloudin);
	condrem.setKeepOrganized(true);
	condrem.filter (*cloudout);
		
		
	}
	
	pcl::ConditionOr<pcl::PointXYZRGB>::Ptr cond;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudout;

private:	
	
	
};

}


 int main(){

   
  
  
  
  
	//Import
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudin (new pcl::PointCloud<pcl::PointXYZRGB>);
	
	pcl::PLYReader Reader;
	Reader.read("test.ply", *cloudin);

	  std::cout << "Loaded "
				<< cloudin->width * cloudin->height
				<< " data points from" << "Lapin"<< "with the following fields: "
				<< std::endl;

	/*
	for (size_t i = 0; i < 1000; ++i){
		std::cout << "" << cloud->points[i] << std::endl; //.x:y:z .r:g:b but are 0

	}
	*/


	//HYPER PLAN CONDITIONNAL REMOVAL
	
	clean::Filter filter1(cloudin,pcl::FieldComparison<pcl::PointXYZRGB> ("z", pcl::ComparisonOps::LT, 1.0));
	
   /*
    * //manual
	// build the condition
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudout1 (new pcl::PointCloud<pcl::PointXYZRGB>);

	pcl::ConditionOr<pcl::PointXYZRGB>::Ptr range_cond (new pcl::ConditionOr<pcl::PointXYZRGB> ()); //comaprison And Or
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZRGB> ("z", pcl::ComparisonOps::LT, 1.0))); //les deux en meme temps marche pas :/
	//range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZRGB>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZRGB> ("y", pcl::ComparisonOps::LT, -0.7))); //overlap filters //WIPWIP A VIRER

	// build the filter
	pcl::ConditionalRemoval<pcl::PointXYZRGB> condrem1;
	condrem1.setCondition(range_cond);
	condrem1.setInputCloud (cloudin);
	condrem1.setKeepOrganized(true);
	condrem1.filter (*cloudout1);
    */
    
    
   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudfinal (filter1.cloudout);
   
   
   //VIEWER
   
   //boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   pcl::visualization::PCLVisualizer viewer ("3D Viewer"); //change viewer en shared pointer
   
   viewer.setBackgroundColor (0, 0, 0);
   viewer.addPointCloud<pcl::PointXYZRGB> (cloudfinal, "cloud");
   viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.5, "cloud"); //string id du cloud, ensuite viewport number default0
   viewer.initCameraParameters ();
   viewer.addCoordinateSystem (0.5,0.0,0.0,0.0,"cloud"); //2.0 a la fin //mire
   //X (red), Y (green) and Z (blue) axes THE FUCKING Z IS A LENVERS
   //red x droite, green y vers le bas, z blue vers le mur


   
  
   //PLANE MUR
   	pcl::ModelCoefficients coeffs;
	coeffs.values.push_back(0.0); //a b c
	coeffs.values.push_back(0.0);
	coeffs.values.push_back(-1.0);
	coeffs.values.push_back(1.0); // d = ax0+by0+cz0
	viewer.addPlane (coeffs, "plane1");
	
	/*
	//PLANE TABOURET
	pcl::ModelCoefficients plane_coeff;
	plane_coeff.values.resize (4);    // We need 4 values
	plane_coeff.values[0] = 0.0;
	plane_coeff.values[1] = 1.0;
	plane_coeff.values[2] = 0.0;
	plane_coeff.values[3] = -0.07; //donc z = 1
	viewer.addPlane(plane_coeff, "plane2");
	
	
	Eigen::Vector4f centroid; 
	pcl::compute3DCentroid(*cloud,centroid); 
	std::cout<<"centroid: "<<centroid[0] << " " << centroid[1] << " " << centroid[2] << std::endl; 
	//viewer.addCoordinateSystem (1.0,centroid[0],centroid[1],centroid[2],"cloud");
	*/

	
	
	
	
   
	while (!viewer.wasStopped ())
	{
	  viewer.spinOnce (100);
	}

   
   return 0;
 }
