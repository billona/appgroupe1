#include <iostream>
#include <vector>

#include <boost/make_shared.hpp>

#include <pcl/common/common.h>
#include <pcl/point_types.h>

#include <pcl/io/obj_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <pcl/registration/icp.h>

#include <pcl/visualization/pcl_visualizer.h>

#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/time.h>
#include <pcl/console/print.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/segmentation/sac_segmentation.h>


// Types
/*
typedef pcl::FPFHEstimationOMP<PointNT,PointNT,pcl::FPFHSignature33> FeatureEstimationT;
typedef pcl::PointCloud<pcl::FPFHSignature33> FeatureCloudT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT> ColorHandlerT;
*/

struct PCLView{
	
	PCLView(int id_,std::string name_, std::string path_): 
		id(id_),
		name(name_),
		path(path_),
		cloud(new pcl::PointCloud<pcl::PointXYZRGB>),
		cloudnorm(new pcl::PointCloud<pcl::PointNormal>),
		feats(new pcl::PointCloud<pcl::FPFHSignature33>)
	
	{
		pcl::PLYReader Reader;
		Reader.read(path, *cloud);
		//pcl::computeCentroid(*cloud,center);
		pcl::compute3DCentroid(*cloud,centroid); 
		
		
	}
			
	int id = 0;
	std::string name;
	std::string path;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
	pcl::PointCloud<pcl::PointNormal>::Ptr cloudnorm;
	pcl::PointCloud<pcl::FPFHSignature33>::Ptr feats;
	  
	Eigen::Vector4f centroid; 
	

	
};

std::vector<PCLView> getsameformat(std::vector<PCLView> set){
	std::vector<PCLView> nset;
	nset.reserve(set.size());
	
}


int main (int argc, char** argv)
{

	//file loading
	std::vector<PCLView> views;


	std::string dir = "models/procedtalkie/"; //wipwip arg c arg v
	std::string suffix = "_talkie.ply";
	int plynum = 3;
	
	for(int i = 1 ; i<= plynum; i++){
		
		std::string name = std::to_string(i) + suffix;
		std::string fullpath =  dir + name; //path.substr(1)
		std::cout<<  "Loading " << name  <<std::endl;
		views.emplace_back(i,name,fullpath);
	}	
	
	//ReCenter
	
	//avr centroid
	Eigen::Vector4f avr;
	for( int i = 0; i< views.size();i++){
		avr += views[i].centroid;
	}
	
	std::cout << avr<< std::endl;
	
	for( int i = 0; i< views.size();i++){
		auto offset = Eigen::Affine3f::Identity();
		offset.translation()<< -0.05,0.0,-0.55 ;
		pcl::transformPointCloud (*views[i].cloud, *views[i].cloud, offset);
	}
	
	
	//nope

	
	//Offset
	std::vector<PCLView> views_offset = views;
	int nside = views.size();
	for( int i = 0; i< nside;i++){
		
		//wipwip need pre rotation
		
		//auto prerot = Eigen::Affine3f::Identity();
		//prerot.rotate (Eigen::AngleAxisf (M_PI/2, Eigen::Vector3f::UnitY()));
		
		auto offset = Eigen::Affine3f::Identity();
		offset.translation() << -0.2, 0.0, 0.0; //0.1*i-0.1*0.5*nside
		
		auto rot = Eigen::Affine3f::Identity();
		rot.rotate (Eigen::AngleAxisf (-2*M_PI*i/nside, Eigen::Vector3f::UnitY()));
		
		//pcl::transformPointCloud (*views_offset[i].cloud, *views_offset[i].cloud, prerot);
		pcl::transformPointCloud (*views_offset[i].cloud, *views_offset[i].cloud, offset);
		pcl::transformPointCloud (*views_offset[i].cloud, *views_offset[i].cloud, rot);
		
	}
	

	/*
		
		
	//ROBUST POSE ESTIMATION
	
	//downscale wipwip
	std::cout << "Downscale" << std::endl;
	const float leaf = 0.003;// = 0.005f; //plus bas = plus rapide less points //std::stof(argv[1])
	
	for(auto v : views){
	  pcl::VoxelGrid<pcl::PointXYZRGB> grid; //poitn norm
	  grid.setLeafSize (leaf, leaf, leaf);
	  grid.setInputCloud (v.cloud); //norms and go after the norms???
	  grid.filter (*v.cloud);
	}
	
	//estiamte all cloud norms
		std::cout << "Estimating normals" << std::endl;
	for(auto v : views){
	
		pcl::NormalEstimationOMP<pcl::PointXYZRGB,pcl::PointNormal> nest;
		nest.setRadiusSearch (0.01);
		nest.setInputCloud (v.cloud);
		nest.compute (*v.cloudnorm);
		
		
		std::cout << "\tnorms: "<< v.cloudnorm->points.size () << std::endl;
	}
		


	// Estimate all features
	std::cout << "Estimating features" << std::endl;
	for(auto v : views){
		pcl::FPFHEstimationOMP<pcl::PointXYZRGB,pcl::PointNormal,pcl::FPFHSignature33> fest;
		fest.setRadiusSearch (0.025);
		fest.setInputCloud (v.cloud);
		fest.setInputNormals (v.cloudnorm);
		fest.compute (*v.feats);
		
		std::cout << "feats: "<< v.feats->points.size () << std::endl;
	}
	


	
	
	
	// Perform alignment only on the seventh first
	std::cout << "Perform alignement" << std::endl;
  
	for(int i = 0; i < views.size() ; i++){
		
		pcl::SampleConsensusPrerejective<pcl::PointXYZRGB,pcl::PointXYZRGB,pcl::FPFHSignature33> align;
		align.setInputSource (views[i+1].cloud);
		align.setSourceFeatures (views[i+1].feats);
		align.setInputTarget (views[i].cloud);
		align.setTargetFeatures (views[i].feats);
		align.setMaximumIterations (50000); // Number of RANSAC iterations
		align.setNumberOfSamples (3); // Number of points to sample for generating/prerejecting a pose
		align.setCorrespondenceRandomness (5); // Number of nearest features to use
		align.setSimilarityThreshold (0.9f); // Polygonal edge length similarity threshold
		align.setMaxCorrespondenceDistance (2.5f * leaf); // Inlier threshold //if down sample  * leaf
		align.setInlierFraction (0.25f); // Required inlier fraction for accepting a pose hypothesis
		{
		pcl::ScopeTime t("Alignment");
		align.align (*views[i+1].cloud); //object algned new cloud but lets give the same object
		}

		if (align.hasConverged ())
		{
		std::cout << "Alignment "<< i+1<<" to "<<i  <<" SUCCESS" << std::endl;
		}
		else
		{
		std::cout << "Alignment "<< i+1<<" to "<<i  <<" FAILED" << std::endl;
			return (1); //quit
		}
		
		
		//perform icp
		
		pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
		icp.setInputSource(views[i+1].cloud);
		icp.setInputTarget(views[i].cloud);
		
		//pcl::PointCloud<pcl::PointXYZRGB> Final;
		icp.align(*views[i+1].cloud);
		
		if(icp.hasConverged()){
			std::cout << "has converged:" << icp.hasConverged() << " score: " <<	  
			icp.getFitnessScore() << std::endl;
			std::cout << icp.getFinalTransformation() << std::endl;
		}else{
			std::cout << "Alignment ICP "<< i+1<<" to "<<i  <<" ICP FAILED" << std::endl;
			return (1); //quit
			
		}

	  
	}
  
  
  */
  
	//VIsualisation
	
	//VIEWER
	pcl::visualization::PCLVisualizer viewer ("3D Viewer");
	viewer.setBackgroundColor (0, 0, 0);
	viewer.initCameraParameters ();
	viewer.addCoordinateSystem (1.0,0.0,0.0,0.0,"cloudcs"); //scale x y z 2.0 a la fin //mire
	
	/*
	//test pose
	viewer.addPointCloud<pcl::PointXYZRGB>(views[0].cloud, views[0].name);
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.5, views[0].name);
	
	viewer.addPointCloud<pcl::PointXYZRGB>(views[1].cloud, views[1].name);
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.5, views[1].name);

	*/
	
	int testlol = 0;
	//show all
	for(auto v : views){ //_offset
		
		if(testlol == 4){
			break;
		}
		
		viewer.addPointCloud<pcl::PointXYZRGB>(v.cloud, v.name);
		viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.5, v.name);
		testlol++;
		
	}
	
	
	
	//9 is view from dows and 10 view from p
	
	while (!viewer.wasStopped ())
	{
	  viewer.spinOnce (100);
	}
	
	
	
	
	
	

 return (0);
}
