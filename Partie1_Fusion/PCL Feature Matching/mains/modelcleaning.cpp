#include <iostream>
#include <string>

#include<cleaning.h>

#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/visualization/cloud_viewer.h>
//#include <pcl/io/io.h>

#include <pcl/common/common.h>
#include <pcl/io/obj_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <pcl/ml/kmeans.h>
#include <pcl/common/centroid.h>


#include <vector>
#include <pcl/point_types.h>

#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>

#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing_rgb.h>

#include<string>
//#include <filesystem>
//namespace fs = std::filesystem;



int
main (int argc, char** argv)
{
  pcl::search::Search <pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);

  pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud <pcl::PointXYZRGB>);
  
  pcl::PLYReader Reader;
		Reader.read("models/raw/lapin/1.ply", *cloud);
  

  pcl::IndicesPtr indices (new std::vector <int>);
  pcl::PassThrough<pcl::PointXYZRGB> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.0, 1.0);
  pass.filter (*indices);

  pcl::RegionGrowingRGB<pcl::PointXYZRGB> reg;
  reg.setInputCloud (cloud);
  reg.setIndices (indices);
  reg.setSearchMethod (tree);
  reg.setDistanceThreshold (10);
  reg.setPointColorThreshold (2);//6
  reg.setRegionColorThreshold (5);//5
  reg.setMinClusterSize (600);

  std::vector <pcl::PointIndices> clusters;
  reg.extract (clusters);

  pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();
  
  
	pcl::visualization::PCLVisualizer viewer ("3D Viewer");
	viewer.setBackgroundColor (0, 0, 0);
	viewer.initCameraParameters ();
	viewer.addCoordinateSystem (1.0,0.0,0.0,0.0,"cloudcs"); //scale x y z 2.0 a la fin //mire
	

	viewer.addPointCloud<pcl::PointXYZRGB>(colored_cloud, "cloudcolor");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.5, "cloudcolor");
	

	
	

	
	
	while (!viewer.wasStopped ())
	{
	  viewer.spinOnce (100);
	}




return 0;


}
