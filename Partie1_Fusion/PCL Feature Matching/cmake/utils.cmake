##Check if LINUX or MSYS or GIT-BASH
set(TEST_CMD "tty")
execute_process(COMMAND ${TEST_CMD} RESULT_VARIABLE CMD_ERROR OUTPUT_QUIET ERROR_QUIET)

if(${CMD_ERROR} EQUAL 0)
	set(CURRENT_SHELL_UNIX TRUE)##already in aprent scope ##wipwip is i scope wide at cmake list that calls the utils true inclusion, add_compile_definitions
	set(CURRENT_SHELL_COLOR TRUE)##already in aprent scope
else()
	##soemtimes false and no comparison?
endif()
unset(TEST_CMD)

##SET COLORS UTILS
if(${CURRENT_SHELL_UNIX})
	string(ASCII 27 Esc)
	set(COLOR_RESET			"${Esc}[m")
	set(COLOR_BOLD			"${Esc}[1m")
	set(COLOR_NONE			"")
	set(COLOR_RED			"${Esc}[31m")
	set(COLOR_GREEN			"${Esc}[32m")
	set(COLOR_YELLOW		"${Esc}[33m")
	set(COLOR_BLUE			"${Esc}[34m")
	set(COLOR_MAGENTA		"${Esc}[35m")
	set(COLOR_CYAN			"${Esc}[36m")
	set(COLOR_WHITE			"${Esc}[37m")
	set(COLOR_BOLDRED		"${Esc}[1;31m")
	set(COLOR_BOLDGREEN		"${Esc}[1;32m")
	set(COLOR_BOLDYELLOW	"${Esc}[1;33m")
	set(COLOR_BOLDBLUE		"${Esc}[1;34m")
	set(COLOR_BOLDMAGENTA	"${Esc}[1;35m")
	set(COLOR_BOLDCYAN		"${Esc}[1;36m")
	set(COLOR_BOLDWHITE		"${Esc}[1;37m")
	
	##if color doesnt exist still itnerpret it, somewhatt error prone
	FUNCTION(HORZBAR COLOR)
		set(COLOR "${${COLOR}}") ##interpretation
		Message(STATUS "\n${COLOR}==================================================${COLOR_RESET}")
	ENDFUNCTION(HORZBAR)
	
	FUNCTION(HORZBART COLOR TITLE)
		set(COLOR "${${COLOR}}") ##interpretation
		Message(STATUS "\n${COLOR}${TITLE}==================================================${COLOR_RESET}")
		##wipwip lenTITLE and align = or add spaces up to 10, max title = 10 error smaller title
	ENDFUNCTION(HORZBART)
	
	FUNCTION(HORZBARST COLOR TITLE)
		set(COLOR "${${COLOR}}") ##interpretation
		Message(STATUS "${COLOR}===${TITLE}===${COLOR_RESET}")
	ENDFUNCTION(HORZBARST)


else()

	set(COLOR_RESET			"")
	set(COLOR_BOLD			"")
	set(COLOR_NONE			"")
	set(COLOR_RED			"")
	set(COLOR_GREEN			"")
	set(COLOR_YELLOW		"")
	set(COLOR_BLUE			"")
	set(COLOR_MAGENTA		"")
	set(COLOR_CYAN			"")
	set(COLOR_WHITE			"")
	set(COLOR_BOLDRED		"")
	set(COLOR_BOLDGREEN		"")
	set(COLOR_BOLDYELLOW	"")
	set(COLOR_BOLDBLUE		"")
	set(COLOR_BOLDMAGENTA	"")
	set(COLOR_BOLDCYAN		"")
	set(COLOR_BOLDWHITE		"")

	FUNCTION(HORZBAR COLOR)
		Message(STATUS "\n==================================================")
	ENDFUNCTION(HORZBAR)
	
	FUNCTION(HORZBART COLOR TITLE)
		Message(STATUS "\n=========================${TITLE}=========================")
	ENDFUNCTION(HORZBART)
	
	FUNCTION(HORZBARST COLOR TITLE)
		Message(STATUS "===${TITLE}===")
	ENDFUNCTION(HORZBARST)

endif()

### FUNCTION DECLARATION

FUNCTION(FULLVARDUMP)

	get_cmake_property(_variableNames VARIABLES)
	list (SORT _variableNames)
	foreach (_variableName ${_variableNames})
		message(STATUS "${_variableName}=${${_variableName}} ${COLOR_RESET}")
		##color terminal bleeding fix
		##need to print literraly color shit
	endforeach()

ENDFUNCTION(FULLVARDUMP)
