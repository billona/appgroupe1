# APP Groupe 1 
Voici le dépot de rendu du Groupe d'APP1 composé de :

* Arnaud Billon
* Benjamin Lavie
* Olivier Bonnet
* Youssef Bendagha
* Valentin Bruley
* Yann Tireau


# Contenu

[Partie 1 Fusion](Partie1_Fusion): Fusion et Recallage de Nuage de points
* [Rapport](Partie1_Fusion/Rapport.md) : Le Rapport de la Partie 1 
* [Compte Rendus](Partie1_Fusion/compterendus) : Les Comptes rendus des séances Allez et Retour.

[Partie 2 Maillage](Partie2_Maillage): Maillage, triangulation des nuages de points
* [Rapport](Partie2_Maillage/Rapport.md) : Le Rapport de la Partie 2
* [Compte Rendus](Partie2_Maillage/compterendus) : Les Comptes rendus des séances Allez et Retour.


